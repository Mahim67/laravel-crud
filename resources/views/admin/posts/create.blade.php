@extends('master')

@section('content')

<div class="container">
    <div class="card">
        <div class="card-header">
            
            <a href="{{route('posts.index')}}" class="btn btn-primary float-right"> 
             Read Page </a>
        </div>
        <div class="card-body w-60%">


            <form action="{{route('posts.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <input class="form-control @error('title')is invalid @enderror" type="text" placeholder="Enter Title" name="title">
                    @error('title')
                    <p class="alert alert-denger">{{$message}}</p>
                    @enderror
                </div>

                <div class="form-group">
                    <input class="form-control @error('author')is invalid @enderror" type="text" placeholder="Enter Author Name" name="author">
                    @error('author')
                    <p class="alert alert-denger">{{$message}}</p>
                    @enderror
                </div>

                <div class="form-group">
                    <textarea class="form-control @error('description')is invalid @enderror" id="exampleFormControlTextarea1" placeholder="Description" rows="3" name="description"></textarea>
                    @error('description')
                    <p class="alert alert-denger">{{$message}}</p>
                    @enderror
                </div>

                <button type="submit" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection