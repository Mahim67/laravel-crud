@extends('master')

@section('content')

<div class="container">
    <div class="card">
        <div class="card-header">
            
            <a href="{{route('posts.index')}}" class="btn btn-primary float-right"> 
             Back </a>
        </div>
        <div class="card-body w-60%">


            <form action="{{route('posts.update',$data->id)}}" method="POST">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <input class="form-control" type="text" placeholder="Enter Title" name="title" value="{{$data->title}}">
                   
                </div>

                <div class="form-group">
                    <input class="form-control" type="text" placeholder="Enter Author Name" name="author" value="{{$data->author}}">
                
                </div>

                <div class="form-group">
                    <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="Description" rows="3" name="description"> {{$data->description}} </textarea>
                    
                </div>

                <button type="submit" class="btn btn-success">Update</button>
            </form>
        </div>
    </div>
</div>

@endsection