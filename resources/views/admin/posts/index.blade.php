@extends('master')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <a class="btn btn-success float-right" href="{{route('posts.create')}}">Create Page</a>
        </div>
        <div class="card-body">
        <table class="table table-bordered text-center">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Title</th>
                    <th scope="col">Author</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $row)
                <tr> 
                    <th scope="row">{{$row->id}}</th>
                    <td>{{$row->title}}</td>
                    <td>{{$row->author}}</td>
                    <td class="justify-content-center d-flex">
                        <!-- show button -->
                        <a class="btn btn-success" href="{{route('posts.show', $row->id)}}">Show</a>
                        <!-- edit button -->
                        <a class="btn btn-info" href="{{route('posts.edit', $row->id)}}">Edit</a>
                        <!-- delete buton -->
                        <form action="{{route('posts.destroy',$row->id)}}" method="post" onsubmit="return confirm('Are You Sure?')" class="deleteForm m-0">
                            @csrf
                            @method('delete')
                            <button  type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
                
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection