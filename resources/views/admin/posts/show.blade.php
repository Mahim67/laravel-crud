@extends('master')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <a href="{{route('posts.index')}}" class="btn btn-primary float-right"> 
             Read Page </a>
        </div>
        <div class="card-body">
            <table class="table table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Title</th>
                        <th scope="col">Author</th>
                        <th scope="col">Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$data->title}}</td>
                        <td>{{$data->author}}</td>
                        <td>{{$data->description}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
@endsection