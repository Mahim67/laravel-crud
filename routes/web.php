<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('admin/posts','PostController');

Route::get('/', 'SiteController@home');
// Route::post('/', 'SiteController@formHandler');
// Route::get('/contact', 'SiteController@contact');
// Route::post('/contact', 'SiteController@contactHandler');
// Route::get('/contact/{id}', 'SiteController@');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
